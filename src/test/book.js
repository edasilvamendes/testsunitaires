// const chaiHttp = require('chai-http');
// const chaiNock = require('chai-nock');
// const chaiAsPromised = require('chai-as-promised');
// const path = require('path');
// const nock = require('nock');
// const chai = require('chai')
// const expect = chai.expect
// const server = require('../server');
// const resetDatabase = require('../utils/resetDatabase');

import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import chaiNock from 'chai-nock';
import chaiAsPromised from 'chai-as-promised';
import path from 'path';
import nock from 'nock';
import server from '../server';
import resetDatabase from '../utils/resetDatabase';

chai.use(chaiHttp);
chai.use(chaiNock);
chai.use(chaiAsPromised);

// tout les packages et fonction nescessaire au test sont importé ici, bon courage

// fait les Tests d'integration en premier
describe("Test Integration",()=>{
    describe("Empty DB",()=>{
        beforeEach(()=>{
            const initialStructure = {
                books: []
            };
            const pathBooks = path.join(__dirname, '../data/books.json');
            resetDatabase(pathBooks,initialStructure);
        });
        describe("Route /GET",()=>{
            it("Response body should be an object", (done) => {
                chai.request('http://localhost:8080')
                .get('/book')
                .end((err,res) => {
                    expect(res.body).to.be.a('object');
                    done();
                });
            });
            it("Response should have status 200", (done) => {
                chai.request('http://localhost:8080')
                .get('/book')
                .end((err,res) => {
                    expect(res).to.have.status('200');
                    done();
                });
            });
            it("Books key should be an Array", (done) => {
                chai.request('http://localhost:8080')
                .get('/book')
                .end((err,res) => {
                    expect(res.body.books).to.be.a('array');
                    done();
                });
            });
            
            it("Books array should be empty", (done) => {
                chai.request('http://localhost:8080')
                .get('/book')
                .end((err,res) => {
                    expect(res.body.books).to.be.empty;
                    done();
                });
            });
            
        });
    
        describe("Route /POST",()=>{
            it("Response should have status 200", (done) => {
                chai.request('http://localhost:8080')
                .post('/book')
                .end((err,res) => {
                    expect(res).to.have.status('200');
                    done();
                });
            });
    
            it("Response body should display 'Book successfully added' message", (done) => {
                chai.request('http://localhost:8080')
                .post('/book')
                .end((err,res) => {
                    expect(res.body.message).to.equal('book successfully added');
                    done();
                });
            });
        });
    })
    
    
    describe('Mocked DB',()=>{
        beforeEach(()=>{
            const initialStructure = {
                books: [{"id":"0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a9","title":"Coco raconte Channel 2","years":1990,"pages":400},{"id":"0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a8","title":"Coco raconte Channel 3","years":2000,"pages":200}]
            };
            const pathBooks = path.join(__dirname, '../data/books.json');
            resetDatabase(pathBooks,initialStructure);
        });
        describe("Route /PUT/:id",()=>{
            it("Response should have status 200", (done) => {
                chai.request('http://localhost:8080')
                .put('/book/0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a9')
                .end((err,res) => {
                    expect(res).to.have.status('200');
                    done();
                });
            });
            it("Response body should display 'book successfully updated' message", (done) => {
                chai.request('http://localhost:8080')
                .put('/book/0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a9')
                .end((err,res) => {
                    expect(res.body.message).equal('book successfully updated');
                    done();
                });
            });
        });
        describe("Route /DELETE/:id",()=>{
            it("Response should have status 200", (done) => {
                chai.request('http://localhost:8080')
                .delete('/book/0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a9')
                .end((err,res) => {
                    expect(res).to.have.status('200');
                    done();
                });
            });
            it("Response body should display 'book successfully deleted' message", (done) => {
                chai.request('http://localhost:8080')
                .delete('/book/0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a9')
                .end((err,res) => {
                    expect(res.body.message).equal('book successfully deleted');
                    done();
                });
            });
        });
        describe("Route /GET/:id",()=>{
            it("Resposne should have status 200", (done) => {
                chai.request('http://localhost:8080')
                .get('/book/0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a9')
                .end((err,res) => {
                    expect(res).to.have.status('200');
                    done();
                });
            });
            it("Response body should contain 'book fetched' message", (done) => {
                chai.request('http://localhost:8080')
                .get('/book/0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a9')
                .end((err,res) => {
                    expect(res.body.message).equal('book fetched');
                    done();
                });
            });           
            it("Response body should be an object", (done) => {
                chai.request('http://localhost:8080')
                .get('/book/0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a9')
                .end((err,res) => {
                    expect(res.body).to.be.a('object');
                    done();
                });
            });
            it("Title key should be a String", (done) => {
                chai.request('http://localhost:8080')
                .get('/book/0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a9')
                .end((err,res) => {
                    expect(res.body.book.title).to.be.a('string');
                    done();
                });
            });
            it("Title key should be equal to DBmock", (done) => {
                chai.request('http://localhost:8080')
                .get('/book/0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a9')
                .end((err,res) => {
                    expect(res.body.book.title).equal('Coco raconte Channel 2');
                    done();
                });
            });
            it("Year key should be a number", (done) => {
                chai.request('http://localhost:8080')
                .get('/book/0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a9')
                .end((err,res) => {
                    expect(res.body.book.years).to.be.a('number');
                    done();
                });
            });
            it("Year key should be equal to DBmock", (done) => {
                chai.request('http://localhost:8080')
                .get('/book/0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a9')
                .end((err,res) => {
                    expect(res.body.book.years).equal(1990);
                    done();
                });
            });
            it("Pages key should be a number", (done) => {
                chai.request('http://localhost:8080')
                .get('/book/0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a9')
                .end((err,res) => {
                    expect(res.body.book.pages).to.be.a('number');
                    done();
                });
            });
            it("Pages key should be equal to DBmock", (done) => {
                chai.request('http://localhost:8080')
                .get('/book/0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a9')
                .end((err,res) => {
                    expect(res.body.book.pages).equal(400);
                    done();
                });
            });
        });
    })
})

describe("Test unitaires",()=>{
    describe("Empty DB", () => {
        beforeEach(()=>{
            const initialStructure = {
                books: []
            };
            const pathBooks = path.join(__dirname, '../data/books.json');
            resetDatabase(pathBooks,initialStructure);
        });

        describe("Route /GET", () => {
            it('TEST GET BOOK', (done) => {
                var book = [];
                var interceptornock;

                interceptornock = nock('http://localhost:8080').get('/book').reply(200, {book});

                chai.request('http://localhost:8080')
                    .get('/book')
                    .end((err,res) => {
                        expect(res.body.book).to.be.a('array');
                        expect(res).to.have.status('200');
                        done();
                    });
            });
        });

        describe("Route /POST", () => {
            it('TEST POST BOOK', (done) => {
                var message = "book successfully added";
                var interceptornock;

                interceptornock = nock('http://localhost:8080').post('/book').reply(200, {message});

                chai.request('http://localhost:8080')
                    .post('/book')
                    .end((err,res) => {
                        expect(res).to.have.status('200');
                        expect(res.body.message).to.equal('book successfully added');
                        done();
                    });
            });
        });
        /*
        describe("Route /GET", () => {
            it('TEST GET BOOK 400', (done) => {
                var book = [];
                var interceptornock;

                interceptornock = nock('http://localhost:8080').get('/book').reply(400, {book});
                
                chai.request('http://localhost:8080')
                    .get('/book')
                    .end((err,res) => {
                        expect(res).to.have.status('400');
                        expect(res.body.message).to.equal('error fetching books');
                        done();
                });
            });
        });

        describe("Route /POST", () => {
            it('TEST POST BOOK 400', (done) => {
                var message = "book successfully added";
                var interceptornock;

                interceptornock = nock('http://localhost:8080').post('/book').reply(400, {message});
                
                chai.request('http://localhost:8080')
                    .post('/book')
                    .end((err,res) => {
                        expect(res).to.have.status('400');
                        expect(res.body.message).to.equal('error adding the book');
                        done();
                });
            });
        });
    */
    });


    describe("DB MOCKED", () => {

        beforeEach(()=>{
            const initialStructure = {
                books:[{"id":"0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a9","title":"Coco raconte Channel 2","years":1990,"pages":400}, {"id":"0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a6","title":"Coco raconte Channel 2","years":1990,"pages":400}]
            };
            const pathBooks = path.join(__dirname, '../data/books.json');
            resetDatabase(pathBooks,initialStructure);
        });

        describe("Route /PUT", () => {
            it('TEST PUT BOOK', (done) => {
                var message = "book successfully updated";
                var interceptornock;

                interceptornock = nock('http://localhost:8080/').put('/book/0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a9').reply(200, {message});

                chai.request('http://localhost:8080')
                    .put('/book/0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a9')
                    .end((err,res) => {
                        expect(res).to.have.status('200');
                        expect(res.body.message).to.equal('book successfully updated');
                        done();
                    });
            });
        });

        describe("Route /DELETE", () => {
            it('TEST DELETE BOOK', (done) => {
                var message = "book successfully deleted";
                var interceptornock;

                interceptornock = nock('http://localhost:8080').delete('/book/0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a6').reply(200, {message});

                chai.request('http://localhost:8080')
                    .delete('/book/0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a6')
                    .end((err,res) => {
                        expect(res).to.have.status('200');
                        expect(res.body.message).to.equal('book successfully deleted');
                        done();
                    });
            });
        });
        /*
        describe("Route /PUT", () => {
            it('TEST PUT BOOK', (done) => {
                var message = "book successfully updated";
                var interceptornock;

                interceptornock = nock('http://localhost:8080/').put('/book/0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a9').reply(400, {message});
                
                chai.request('http://localhost:8080')
                    .put('/book/0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a9')
                    .end((err,res) => {
                        expect(res).to.have.status('400');
                        expect(res.body.message).to.equal('error updating the book');
                        done();
                });
            });
        });

        describe("Route /DELETE", () => {
            it('TEST DELETE BOOK', (done) => {
                var message = "book successfully deleted";
                var interceptornock;
                
                interceptornock = nock('http://localhost:8080').delete('/book/0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a6').reply(400, {message});
                
                chai.request('http://localhost:8080')
                    .delete('/book/0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a6')
                    .end((err,res) => {
                        expect(res).to.have.status('400');
                        expect(res.body.message).to.equal('error deleting the book');
                        done();
                });
            });
        });
        */

    });

})

